const path = require('path');
const fs = require('fs')

test('main index.html file exists', () => {
  const filePath = path.join(__dirname, "index.html")
  expect(fs.existsSync(filePath)).toBeTruthy();
});

test('app.js exists', () => {
  const filePath = path.join(__dirname, "..", "client", "js", "app.js")
  expect(fs.existsSync(filePath)).toBeTruthy();
});

test('package.json file exists', () => {
  const filePath = path.join(__dirname, "package.json")
  expect(fs.existsSync(filePath)).toBeTruthy();
});